<H1>
Hospital Management
</H1>
<hr>

To run the application, issue below command:  
   mvn spring-boot:run -Dspring-boot.run.profiles=dev
   
For API documentation, please navigate to below url:  
   http://localhost:8080/hospital/swagger-ui.html

