package com.practice.hospital.dao;

import com.practice.hospital.domain.entity.AppointmentDetailView;
import com.practice.hospital.domain.entity.AppointmentEO;
import com.practice.hospital.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AppointmentDAOImpl implements AppointmentDAO {

    private static Logger LOGGER = LoggerFactory.getLogger(AppointmentDAOImpl.class);

    private static String ID_COLUMN = "id";
    private static String NAME_COLUMN = "full_name";
    private static String PATIENT_ID_COLUMN = "patient_id";
    private static String APPOINTMENT_DATE_COLUMN  = "appointment_date";
    private static String GET_ALL_APPOINTMENTS_DETAILS = "select p." + ID_COLUMN + ", p." + NAME_COLUMN
            + ", a." + ID_COLUMN + ", a." + APPOINTMENT_DATE_COLUMN + " from appointment a join patient p on a."
            + PATIENT_ID_COLUMN + " = p." + ID_COLUMN;
    private static String CREATE_APPOINTMENT = "insert into appointment(" + PATIENT_ID_COLUMN + ","
            + APPOINTMENT_DATE_COLUMN + ") values(:"+ PATIENT_ID_COLUMN +",:" + APPOINTMENT_DATE_COLUMN + ")";

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    KeyHolder keyHolder;

    @Override
    public List<AppointmentDetailView> getAllAppointments() throws BusinessException {
        List<AppointmentDetailView> appointmentDetailViews;
        try {
            appointmentDetailViews =
                    jdbcTemplate.query(GET_ALL_APPOINTMENTS_DETAILS, new AppointmentDetailRowMapper());
        } catch (Exception ex) {
            LOGGER.error("Error in getting appointments: {}", ex.getMessage());
            throw new BusinessException("Error in getting appointments");
        }

        return appointmentDetailViews;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public List<Integer> createAppointment(List<AppointmentEO> appointments) throws BusinessException {
        List<Integer> appointmentIds = new ArrayList<>();
        try {
            for (AppointmentEO appointment : appointments) {
                jdbcTemplate.update(CREATE_APPOINTMENT, getParamMap(appointment), keyHolder);
                if (keyHolder.getKey() != null) {
                    appointmentIds.add(keyHolder.getKey().intValue());
                }
            }
        }  catch (DataIntegrityViolationException ex) {
            LOGGER.error("Appointment could not be created because the patient doesn't exist");
            throw new BusinessException("Appointment could not be created because the patient doesn't exist");
        }  catch (DataAccessException ex) {
            LOGGER.error("Error in appointment creation: {}", ex.getMessage());
            throw ex;
        }
        return appointmentIds;
    }

    private MapSqlParameterSource getParamMap(AppointmentEO appointment) {
        MapSqlParameterSource paramMap = new MapSqlParameterSource();
        paramMap.addValue(PATIENT_ID_COLUMN, appointment.getPatientId());
        paramMap.addValue(APPOINTMENT_DATE_COLUMN, appointment.getDate());
        return paramMap;
    }

    static class AppointmentDetailRowMapper implements RowMapper<AppointmentDetailView> {

        @Override
        public AppointmentDetailView mapRow(ResultSet resultSet, int i) throws SQLException {
            AppointmentDetailView detailView =new AppointmentDetailView();
            detailView.setPatientId(resultSet.getInt(1));
            detailView.setPatientName(resultSet.getString(2));
            detailView.setAppointmentId(resultSet.getInt(3));
            detailView.setAppointmentDate(resultSet.getTimestamp(4));
            return detailView;
        }
    }
}
