package com.practice.hospital.dao;

import com.practice.hospital.domain.entity.PatientEO;
import com.practice.hospital.exception.BusinessException;

import java.util.List;

public interface PatientDAO {
    List<PatientEO> getAllPatients() throws BusinessException;
    List<Integer> addPatient(List<PatientEO> patients) throws BusinessException;
}
