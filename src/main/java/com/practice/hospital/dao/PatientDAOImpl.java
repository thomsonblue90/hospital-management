package com.practice.hospital.dao;

import com.practice.hospital.domain.entity.PatientEO;
import com.practice.hospital.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PatientDAOImpl implements PatientDAO {

    private static Logger LOGGER = LoggerFactory.getLogger(PatientDAOImpl.class);
    private static String NAME_COLUMN = "full_name";
    private static String GET_ALL_PATIENTS = "select * from patient";
    private static String ADD_PATIENT = "insert into patient(" + NAME_COLUMN + ") values(:"+ NAME_COLUMN +")";

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    KeyHolder keyHolder;

    @Override
    public List<PatientEO> getAllPatients() throws BusinessException {
        List<PatientEO> result;
        try {
            result = jdbcTemplate.query(GET_ALL_PATIENTS, new BeanPropertyRowMapper<>(PatientEO.class));
        } catch (Exception ex) {
            LOGGER.error("Error in getting patients: {}", ex.getMessage());
            throw new BusinessException("Error in getting patients");
        }
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public List<Integer> addPatient(List<PatientEO> patients) throws BusinessException {
        List<Integer> patientIds = new ArrayList<>();
        try {
            for (PatientEO patient : patients) {
                jdbcTemplate.update(ADD_PATIENT,getParamMap(patient), keyHolder);
                if (keyHolder.getKey() != null) {
                    patientIds.add(keyHolder.getKey().intValue());
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error in adding patients: {}", ex.getMessage());
            throw new BusinessException("Error in adding patients");
        }
        return patientIds;
    }

    private MapSqlParameterSource getParamMap(PatientEO patient) throws DataAccessException {
        MapSqlParameterSource paramMap = new MapSqlParameterSource();
        paramMap.addValue(NAME_COLUMN, patient.getFullName());
        return paramMap;
    }

}
