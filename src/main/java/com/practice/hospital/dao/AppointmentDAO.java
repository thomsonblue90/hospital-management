package com.practice.hospital.dao;

import com.practice.hospital.domain.entity.AppointmentDetailView;
import com.practice.hospital.domain.entity.AppointmentEO;
import com.practice.hospital.exception.BusinessException;

import java.util.List;

public interface AppointmentDAO {
    List<AppointmentDetailView> getAllAppointments() throws BusinessException;
    List<Integer> createAppointment(List<AppointmentEO> appointments) throws BusinessException;
}
