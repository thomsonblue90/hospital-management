package com.practice.hospital.service;

import com.practice.hospital.domain.dto.AppointmentCreateDTO;
import com.practice.hospital.domain.dto.AppointmentDetailDTO;
import com.practice.hospital.exception.BusinessException;
import com.practice.hospital.manager.AppointmentManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    AppointmentManager appointmentManager;

    @Override
    public List<AppointmentDetailDTO> getAllAppointments() throws BusinessException {
        return appointmentManager.getAllAppointments();
    }

    @Override
    public List<Integer> createAppointment(List<AppointmentCreateDTO> appointments) throws BusinessException {
        return appointmentManager.createAppointment(appointments);
    }
}
