package com.practice.hospital.service;

import com.practice.hospital.domain.dto.PatientDTO;
import com.practice.hospital.exception.BusinessException;

import java.util.List;

public interface PatientService {
    List<PatientDTO> getAllPatients() throws BusinessException;
    List<Integer> addPatient(List<PatientDTO> patients) throws BusinessException;
}
