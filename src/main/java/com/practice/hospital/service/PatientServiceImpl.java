package com.practice.hospital.service;

import com.practice.hospital.domain.dto.PatientDTO;
import com.practice.hospital.exception.BusinessException;
import com.practice.hospital.manager.PatientManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    PatientManager patientManager;

    @Override
    public List<PatientDTO> getAllPatients() throws BusinessException {
        return patientManager.getAllPatients();
    }

    @Override
    public List<Integer> addPatient(List<PatientDTO> patients) throws BusinessException {
        return patientManager.addPatient(patients);
    }
}
