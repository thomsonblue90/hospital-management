package com.practice.hospital.service;

import com.practice.hospital.domain.dto.AppointmentCreateDTO;
import com.practice.hospital.domain.dto.AppointmentDetailDTO;
import com.practice.hospital.domain.dto.PatientDTO;
import com.practice.hospital.exception.BusinessException;

import java.util.List;

public interface AppointmentService {
    List<AppointmentDetailDTO> getAllAppointments() throws BusinessException;
    List<Integer> createAppointment(List<AppointmentCreateDTO> appointments) throws BusinessException;
}
