package com.practice.hospital.controller;

import com.practice.hospital.domain.dto.ApiResultDTO;
import com.practice.hospital.domain.dto.PatientDTO;
import com.practice.hospital.exception.BusinessException;
import com.practice.hospital.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/patients")
public class PatientController extends AbstractController {

    @Autowired
    PatientService patientService;

    @GetMapping
    public ResponseEntity<ApiResultDTO> getAllPatients() throws BusinessException {
        List<PatientDTO> patients = patientService.getAllPatients();
        if (patients.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(getApiResult(patients, HttpStatus.OK));
    }

    @PostMapping
    public ResponseEntity<ApiResultDTO> addPatients(@RequestBody List<PatientDTO> patientDTOs) throws BusinessException {
        List<Integer> patientIds = patientService.addPatient(patientDTOs);
        if (patientIds.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(getApiResult(patientIds, HttpStatus.CREATED));
    }
}
