package com.practice.hospital.controller;

import com.practice.hospital.domain.dto.ApiResultDTO;
import org.springframework.http.HttpStatus;

import java.util.Date;

public abstract class AbstractController {
    protected ApiResultDTO getApiResult(Object response, HttpStatus status) {
        ApiResultDTO apiResultDTO = new ApiResultDTO();
        apiResultDTO.setStatus(status.value());
        apiResultDTO.setTimestamp(new Date());
        apiResultDTO.setResponse(response);
        return apiResultDTO;
    }
}
