package com.practice.hospital.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeartBeatController {

    @GetMapping("/heartbeat")
    public ResponseEntity<String> getHeartBeat() {
        return ResponseEntity.ok("Alive...");
    }
}
