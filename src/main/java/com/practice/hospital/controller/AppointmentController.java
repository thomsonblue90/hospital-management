package com.practice.hospital.controller;

import com.practice.hospital.domain.dto.ApiResultDTO;
import com.practice.hospital.domain.dto.AppointmentCreateDTO;
import com.practice.hospital.domain.dto.AppointmentDetailDTO;
import com.practice.hospital.exception.BusinessException;
import com.practice.hospital.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/appointments")
public class AppointmentController extends AbstractController {

    @Autowired
    AppointmentService appointmentService;

    @GetMapping
    public ResponseEntity<ApiResultDTO> getAllAppointments() throws BusinessException {
        List<AppointmentDetailDTO> appointments = appointmentService.getAllAppointments();
        if (appointments.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(getApiResult(appointments, HttpStatus.OK));
    }

    @PostMapping
    public ResponseEntity<ApiResultDTO> createAppointments(@RequestBody List<AppointmentCreateDTO> appointments)
            throws BusinessException {
        List<Integer> appointmentIds = appointmentService.createAppointment(appointments);
        if (appointmentIds.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(getApiResult(appointmentIds, HttpStatus.CREATED));
    }
}
