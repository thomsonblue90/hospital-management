package com.practice.hospital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.TimeZone;

@SpringBootApplication
public class HospitalApp extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(HospitalApp.class, args);
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}
