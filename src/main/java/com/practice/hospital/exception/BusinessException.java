package com.practice.hospital.exception;

public class BusinessException extends Exception {
    public BusinessException(String message) {
        super(message);
    }
}
