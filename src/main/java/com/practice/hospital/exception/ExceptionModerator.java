package com.practice.hospital.exception;

import com.practice.hospital.domain.dto.ApiResultDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class ExceptionModerator {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResultDTO> catchGlobalException(Exception exception) {
        ApiResultDTO apiError = new ApiResultDTO();
        apiError.setTimestamp(new Date());
        apiError.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        apiError.setErrorMessage(exception.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(apiError);
    }
}
