package com.practice.hospital.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class AppointmentCreateDTO {
    private Integer patientId;
    private Integer appointmentId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    private Date date;

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Integer appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "AppointmentCreateDTO{" +
                "patientId=" + patientId +
                ", appointmentId=" + appointmentId +
                ", date=" + date +
                '}';
    }
}
