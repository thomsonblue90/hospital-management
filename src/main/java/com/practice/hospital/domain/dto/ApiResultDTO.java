package com.practice.hospital.domain.dto;

import java.util.Date;

public class ApiResultDTO {
    Date timestamp;
    Integer status;
    String errorMessage;
    Object response;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "ApiResultDTO{" +
                "timestamp=" + timestamp +
                ", status=" + status +
                ", errorMessage='" + errorMessage + '\'' +
                ", response=" + response +
                '}';
    }
}
