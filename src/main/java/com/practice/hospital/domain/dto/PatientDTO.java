package com.practice.hospital.domain.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class PatientDTO {


    @NotEmpty(message = "Name may not be empty")
    @Size(min = 2, max = 32, message = "Name must be between 2 and 70 characters long")
    private String fullName;
    private Integer id;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "PatientDTO{" +
                "fullName='" + fullName + '\'' +
                ", id=" + id +
                '}';
    }
}
