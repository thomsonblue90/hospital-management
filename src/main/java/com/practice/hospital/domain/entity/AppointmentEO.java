package com.practice.hospital.domain.entity;

import java.util.Date;

public class AppointmentEO {
    private Integer patientId;
    private Integer appointmentId;
    private Date date;

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Integer appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "AppointmentEO{" +
                "patientId=" + patientId +
                ", appointmentId=" + appointmentId +
                ", date=" + date +
                '}';
    }
}
