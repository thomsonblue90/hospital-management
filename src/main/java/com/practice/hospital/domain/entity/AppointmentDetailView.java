package com.practice.hospital.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class AppointmentDetailView {
    private Integer patientId;
    private String patientName;
    private Integer appointmentId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    private Date appointmentDate;

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public Integer getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Integer appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Date getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    @Override
    public String toString() {
        return "AppointmentDetailView{" +
                "patientId=" + patientId +
                ", patientName='" + patientName + '\'' +
                ", appointmentId=" + appointmentId +
                ", appointmentDate=" + appointmentDate +
                '}';
    }
}
