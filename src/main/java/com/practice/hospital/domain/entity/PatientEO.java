package com.practice.hospital.domain.entity;

public class PatientEO {
    private String fullName;
    private Integer id;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "PatientEO{" +
                "fullName='" + fullName + '\'' +
                ", id=" + id +
                '}';
    }
}
