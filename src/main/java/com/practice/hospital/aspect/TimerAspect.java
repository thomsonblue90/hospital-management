package com.practice.hospital.aspect;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class TimerAspect {

    private static Logger LOGGER = LoggerFactory.getLogger(TimerAspect.class);

    @Pointcut("execution(* com.practice.hospital.controller.*.*(..))")
    public void pointCuts(){}

    @Around("pointCuts()")
    public Object profileMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();
        long startTime = System.currentTimeMillis();
        Object output = null;
        output = proceedingJoinPoint.proceed();
        long elapsedTime = System.currentTimeMillis() - startTime;
        LOGGER.info("Total Time for {} is {}ms", proceedingJoinPoint.getSignature().getName(), elapsedTime);
        return output;
    }
}
