package com.practice.hospital.manager;

import com.practice.hospital.domain.dto.AppointmentCreateDTO;
import com.practice.hospital.domain.dto.AppointmentDetailDTO;
import com.practice.hospital.domain.dto.PatientDTO;
import com.practice.hospital.domain.entity.AppointmentDetailView;
import com.practice.hospital.exception.BusinessException;

import java.util.List;

public interface AppointmentManager {
    List<AppointmentDetailDTO> getAllAppointments() throws BusinessException;
    List<Integer> createAppointment(List<AppointmentCreateDTO> appointments) throws BusinessException;
}
