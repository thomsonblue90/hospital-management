package com.practice.hospital.manager;

import com.practice.hospital.dao.AppointmentDAO;
import com.practice.hospital.domain.dto.AppointmentCreateDTO;
import com.practice.hospital.domain.dto.AppointmentDetailDTO;
import com.practice.hospital.domain.entity.AppointmentDetailView;
import com.practice.hospital.domain.entity.AppointmentEO;
import com.practice.hospital.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppointmentManagerImpl implements AppointmentManager {

    @Autowired
    AppointmentDAO appointmentDAO;

    @Override
    public List<AppointmentDetailDTO> getAllAppointments() throws BusinessException {
        List<AppointmentDetailView> appointmentDetailViews = appointmentDAO.getAllAppointments();
        List<AppointmentDetailDTO> appointmentDetailDTOS = convertToDto(appointmentDetailViews);
        return appointmentDetailDTOS;
    }

    @Override
    public List<Integer> createAppointment(List<AppointmentCreateDTO> appointments) throws BusinessException {
        List<Integer> appointmentIds = appointmentDAO.createAppointment(convertToEntity(appointments));
        return appointmentIds;
    }

    private List<AppointmentDetailDTO> convertToDto(List<AppointmentDetailView> appointmentDetailViews) {
        List<AppointmentDetailDTO> appointmentDetailDTOS = new ArrayList<>();
        appointmentDetailViews.forEach(appointmentDetailView -> {
            AppointmentDetailDTO appointmentDetailDTO = new AppointmentDetailDTO();
            appointmentDetailDTO.setPatientId(appointmentDetailView.getPatientId());
            appointmentDetailDTO.setPatientName(appointmentDetailView.getPatientName());
            appointmentDetailDTO.setAppointmentId(appointmentDetailView.getAppointmentId());
            appointmentDetailDTO.setAppointmentDate(appointmentDetailView.getAppointmentDate());
            appointmentDetailDTOS.add(appointmentDetailDTO);
        });
        return appointmentDetailDTOS;
    }

    private List<AppointmentEO> convertToEntity(List<AppointmentCreateDTO> appointments) {
        List<AppointmentEO> appointmentEOS = new ArrayList<>();
        appointments.forEach(appointmentCreateDTO -> {
            AppointmentEO appointmentEO = new AppointmentEO();
            appointmentEO.setPatientId(appointmentCreateDTO.getPatientId());
            appointmentEO.setDate(appointmentCreateDTO.getDate());
            appointmentEOS.add(appointmentEO);
        });
        return appointmentEOS;
    }
}
