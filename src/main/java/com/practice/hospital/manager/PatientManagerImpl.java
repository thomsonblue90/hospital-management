package com.practice.hospital.manager;

import com.practice.hospital.dao.PatientDAO;
import com.practice.hospital.domain.dto.PatientDTO;
import com.practice.hospital.domain.entity.PatientEO;
import com.practice.hospital.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PatientManagerImpl implements PatientManager {

    @Autowired
    PatientDAO patientDAO;

    @Override
    public List<PatientDTO> getAllPatients() throws BusinessException {
        List<PatientEO> patientEOS = patientDAO.getAllPatients();
        List<PatientDTO> patientDTOS = convertToDto(patientEOS);
        return patientDTOS;
    }

    @Override
    public List<Integer> addPatient(List<PatientDTO> patients) throws BusinessException {
        List<Integer> patientIds = new ArrayList<>();
        patientIds = patientDAO.addPatient(convertToEntity(patients));
        return patientIds;
    }

    private List<PatientDTO> convertToDto(List<PatientEO> patientEOS) {
        List<PatientDTO> patientDTOS = new ArrayList<>();
        patientEOS.forEach(patientEO -> {
            PatientDTO patientDTO = new PatientDTO();
            patientDTO.setId(patientEO.getId());
            patientDTO.setFullName(patientEO.getFullName());
            patientDTOS.add(patientDTO);
        });
        return patientDTOS;
    }

    private List<PatientEO> convertToEntity(List<PatientDTO> patientDTOS) {
        List<PatientEO> patientEOS = new ArrayList<>();
        patientDTOS.forEach(patientDTO -> {
            PatientEO patientEO = new PatientEO();
            patientEO.setId(patientDTO.getId());
            patientEO.setFullName(patientDTO.getFullName());
            patientEOS.add(patientEO);
        });
        return patientEOS;
    }
}
