package com.practice.hospital.manager;

import com.practice.hospital.domain.dto.PatientDTO;
import com.practice.hospital.exception.BusinessException;

import java.util.List;

public interface PatientManager {
    List<PatientDTO> getAllPatients() throws BusinessException;
    List<Integer> addPatient(List<PatientDTO> patients) throws BusinessException;
}
