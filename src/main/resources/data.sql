DROP TABLE IF EXISTS patient;
DROP TABLE IF EXISTS appointment;

CREATE TABLE patient (
  id int AUTO_INCREMENT PRIMARY KEY,
  full_name VARCHAR(70) NOT NULL
);

CREATE TABLE appointment (
  id int AUTO_INCREMENT PRIMARY KEY,
  patient_id int NOT NULL REFERENCES patient(id),
  appointment_date datetime NOT NULL
);