package com.practice.hospital.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import com.practice.hospital.HospitalApp;
import com.practice.hospital.dao.PatientDAO;
import com.practice.hospital.domain.dto.PatientDTO;
import com.practice.hospital.domain.entity.PatientEO;
import com.practice.hospital.exception.BusinessException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HospitalApp.class)
@WebAppConfiguration
@AutoConfigureMockMvc
public class PatientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientDAO patientDAO;


    @Test
    public void getAllPatients() throws Exception {
        Mockito.when(patientDAO.getAllPatients()).thenReturn(createPatients());
        mockMvc.perform(MockMvcRequestBuilders.get("/patients")).andDo(MockMvcResultHandlers.print())
                .andExpect(content().string(containsString("Ram Singh")));
    }

    @Test
    public void addPatients() throws Exception {
        Mockito.when(patientDAO.addPatient(Mockito.anyList())).thenReturn(Arrays.asList(100));
        mockMvc.perform(MockMvcRequestBuilders.post("/patients").contentType(MediaType.APPLICATION_JSON)
                .content("[\n" +
                        "\t{\n" +
                        "\t\t\"fullName\": \"Ram Singh\"\n" +
                        "\t},\n" +
                        "\t{\n" +
                        "\t\t\"fullName\": \"Mohan Raj\"\n" +
                        "\t}\n" +
                        "]"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(content().string(containsString("100")));
    }

    private List<PatientEO> createPatients() {
        List<PatientEO> patientEOS = new ArrayList<>();
        patientEOS.add(createPatient("Ram Singh", 1));
        return patientEOS;
    }

    private PatientEO createPatient(String name, Integer id) {
        PatientEO patientEO = new PatientEO();
        patientEO.setFullName(name);
        patientEO.setId(id);
        return patientEO;
    }
}