package com.practice.hospital.dao;

import com.practice.hospital.domain.entity.PatientEO;
import com.practice.hospital.exception.BusinessException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.KeyHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class PatientDAOImplTest {

    private static String GET_ALL_PATIENTS = "select * from patient";
    private static String NAME_COLUMN = "full_name";
    private static String ADD_PATIENT = "insert into patient(" + NAME_COLUMN + ") values(:"+ NAME_COLUMN +")";

    @InjectMocks
    PatientDAOImpl patientDAO;

    @Mock
    NamedParameterJdbcTemplate jdbcTemplate;

    @Mock
    KeyHolder keyHolder;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllPatientsTest() throws BusinessException {
        Mockito.when(jdbcTemplate.query(Mockito.eq(GET_ALL_PATIENTS),
                Mockito.any(BeanPropertyRowMapper.class))).thenReturn(createPatients());
        List<PatientEO> patientEOS = patientDAO.getAllPatients();
        Assert.assertEquals("Size doesn't match", 1, patientEOS.size());
    }

    @Test(expected = BusinessException.class)
    public void getAllPatientsWithExceptionTest() throws BusinessException {
        Mockito.when(jdbcTemplate.query(Mockito.eq(GET_ALL_PATIENTS),
                Mockito.any(BeanPropertyRowMapper.class))).thenThrow(new DataIntegrityViolationException(""));
        List<PatientEO> patientEOS = patientDAO.getAllPatients();
    }

    @Test
    public void addPatientTest() throws BusinessException {
        Mockito.when(jdbcTemplate.update(Mockito.eq(ADD_PATIENT),
                Mockito.any(MapSqlParameterSource.class), Mockito.eq(keyHolder))).thenReturn(1);
        Mockito.when(keyHolder.getKey()).thenReturn(new AtomicInteger(1));
        List<Integer> ids = patientDAO.addPatient(createPatients());
        Assert.assertEquals("Size doesn't match", 1, ids.size());
        Assert.assertEquals("Id doesn't match", 1, ids.get(0).intValue());
    }

    @Test(expected = BusinessException.class)
    public void addPatientExceptionTest() throws BusinessException {
        Mockito.when(jdbcTemplate.update(Mockito.eq(ADD_PATIENT),
                Mockito.any(MapSqlParameterSource.class), Mockito.eq(keyHolder))).thenThrow(new DataIntegrityViolationException(""));
        Mockito.when(keyHolder.getKey()).thenReturn(new AtomicInteger(1));
        List<Integer> ids = patientDAO.addPatient(createPatients());
    }

    private List<PatientEO> createPatients() {
        List<PatientEO> patientEOS = new ArrayList<>();
        patientEOS.add(createPatient("Ram Singh", 1));
        return patientEOS;
    }

    private PatientEO createPatient(String name, Integer id) {
        PatientEO patientEO = new PatientEO();
        patientEO.setFullName(name);
        patientEO.setId(id);
        return patientEO;
    }
}